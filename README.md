To run tests
```
ginkgo -r -v
```

To run coverage
```
go test -coverprofile coverage ./... && go tool cover -html=coverage
```