package main

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/handlers"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/repository"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/service"
	"bitbucket.innouk.music-group.com/cm/secrets-module-go"
	"fmt"
	"go.uber.org/dig"
	"log"
	"net/http"
	"os"
)

var serviceName = "mcloud-service-devices-go"
var environment = os.Getenv("NODE_ENV")

func main() {
	err := secrets.GetSecrets(serviceName, environment)
	if err != nil {
		log.Fatal("Error getting secrets")
	}

	container := dig.New()

	container.Provide(config.NewConfig)
	container.Provide(repositories.NewDeviceMongoRepository)
	container.Provide(repositories.NewModelMongoRepository)
	container.Provide(services.NewDeviceService)
	container.Provide(services.NewModelService)
	container.Provide(handlers.NewDeviceHandlers)
	container.Provide(handlers.NewModelHandlers)
	container.Provide(devices.New)

	container.Invoke(func(svc *devices.Service, config *config.Config) {
		log.Printf("Devices service listening on :%s", config.Port)
		log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", config.Port), svc))
	})
}
