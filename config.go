package config

import (
	"os"
)

type Config struct {
	Env      string
	Port     string
	MongoURL string
	Logstash string
}

func NewConfig() *Config {
	return &Config{
		os.Getenv("NODE_ENV"),
		os.Getenv("PORT"),
		os.Getenv("MONGO_URL"),
		os.Getenv("LOGSTASH_URL"),
	}
}
