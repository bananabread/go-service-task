package mocks

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"encoding/json"
	"github.com/google/uuid"
)

var MockDeviceModel = models.Device{
	UUID:       uuid.New(),
	DeviceName: "Test Device",
	ModelUUID:  uuid.New(),
	Records:    nil,
}

var MockMetadataModel = models.Metadata{
	Metadata: json.RawMessage(`{"foo":"bar"}`),
	Geoip:    json.RawMessage(`{"foo":"bar"}`),
}
