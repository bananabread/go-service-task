// Code generated by mockery v1.0.0. DO NOT EDIT.
package mocks

import mock "github.com/stretchr/testify/mock"
import models "bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"

import uuid "github.com/google/uuid"

// ModelService is an autogenerated mock type for the ModelService type
type ModelService struct {
	mock.Mock
}

// Delete provides a mock function with given fields: _a0
func (_m *ModelService) Delete(_a0 uuid.UUID) (bool, error) {
	ret := _m.Called(_a0)

	var r0 bool
	if rf, ok := ret.Get(0).(func(uuid.UUID) bool); ok {
		r0 = rf(_a0)
	} else {
		r0 = ret.Get(0).(bool)
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uuid.UUID) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetAllModels provides a mock function with given fields:
func (_m *ModelService) GetAllModels() ([]*models.Model, error) {
	ret := _m.Called()

	var r0 []*models.Model
	if rf, ok := ret.Get(0).(func() []*models.Model); ok {
		r0 = rf()
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).([]*models.Model)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func() error); ok {
		r1 = rf()
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// GetByID provides a mock function with given fields: _a0
func (_m *ModelService) GetByID(_a0 uuid.UUID) (*models.Model, error) {
	ret := _m.Called(_a0)

	var r0 *models.Model
	if rf, ok := ret.Get(0).(func(uuid.UUID) *models.Model); ok {
		r0 = rf(_a0)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Model)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uuid.UUID) error); ok {
		r1 = rf(_a0)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}

// Update provides a mock function with given fields: _a0, model
func (_m *ModelService) Update(_a0 uuid.UUID, model *models.Model) (*models.Model, error) {
	ret := _m.Called(_a0, model)

	var r0 *models.Model
	if rf, ok := ret.Get(0).(func(uuid.UUID, *models.Model) *models.Model); ok {
		r0 = rf(_a0, model)
	} else {
		if ret.Get(0) != nil {
			r0 = ret.Get(0).(*models.Model)
		}
	}

	var r1 error
	if rf, ok := ret.Get(1).(func(uuid.UUID, *models.Model) error); ok {
		r1 = rf(_a0, model)
	} else {
		r1 = ret.Error(1)
	}

	return r0, r1
}
