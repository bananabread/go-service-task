package handlers

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/service"
	"bitbucket.innouk.music-group.com/cm/error-module-go/pkg/oops"
	"encoding/json"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"io/ioutil"
	"net/http"
)

type DeviceHandlers interface {
	GetAllDevices(c *gin.Context)
	GetByID(c *gin.Context)
	GetLatestByID(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
	RegisterRoutes(r *gin.Engine, version int)
}

type deviceHandlers struct {
	service services.DeviceService
}

func NewDeviceHandlers(service services.DeviceService) DeviceHandlers {
	return &deviceHandlers{
		service: service,
	}
}

func (h *deviceHandlers) RegisterRoutes(r *gin.Engine, version int) {
	r.GET(fmt.Sprintf("/devices/v%d/", version), h.GetAllDevices)
	r.GET(fmt.Sprintf("/devices/v%d/:uuid", version), h.GetByID)
	r.GET(fmt.Sprintf("/devices/v%d/:uuid/latest", version), h.GetLatestByID)
	r.PUT(fmt.Sprintf("/devices/v%d/:uuid", version), h.Update)
	r.DELETE(fmt.Sprintf("/devices/v%d/:uuid", version), h.Delete)
}

func (h *deviceHandlers) GetAllDevices(c *gin.Context) {
	devices, err := h.service.GetAllDevices()
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"devices": devices,
	})
}

func (h *deviceHandlers) GetByID(c *gin.Context) {
	deviceUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Device UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	device, err := h.service.GetByID(deviceUUID)
	if err != nil {
		oops.NewError(c.Writer, "Device not found", http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"device":  device,
	})
}

func (h *deviceHandlers) GetLatestByID(c *gin.Context) {
	deviceUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Device UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	device, err := h.service.GetLatestByID(deviceUUID)
	if err != nil {
		oops.NewError(c.Writer, "Device not found", http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"device":  device,
	})
}

func (h *deviceHandlers) Update(c *gin.Context) {
	deviceUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Device UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	body, err := ioutil.ReadAll(c.Request.Body)
	device, metadata := models.Device{}, models.Metadata{}

	if err := json.Unmarshal(body, &device); err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("%s", err.Error()), http.StatusInternalServerError)
		return
	}

	if device.DeviceName == "" {
		oops.NewError(c.Writer, "Device name is required", http.StatusBadRequest)
		return
	}

	if err := json.Unmarshal(body, &metadata); err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("%s", err.Error()), http.StatusInternalServerError)
		return
	}

	if metadata.Metadata == nil {
		oops.NewError(c.Writer, "Device name is required", http.StatusBadRequest)
		return
	}

	result, err := h.service.Update(deviceUUID, &metadata, &device)
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"device":  result,
	})
}

func (h *deviceHandlers) Delete(c *gin.Context) {
	deviceUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Device UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	success, err := h.service.Delete(deviceUUID)
	if err != nil {
		oops.NewError(c.Writer, "Device not found", http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": success,
	})
}
