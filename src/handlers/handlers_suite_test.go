package handlers_test

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/mocks"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/handlers"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"bytes"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandlers(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Handlers Suite")
}

var _ = Describe("Device Handlers", func() {
	var (
		testUUID       uuid.UUID
		testUUIDString string
		mockService    mocks.DeviceService
		deviceHandler  handlers.DeviceHandlers
		response       *httptest.ResponseRecorder
		context        *gin.Context
	)

	BeforeEach(func() {
		testUUID = uuid.New()
		testUUIDString = testUUID.String()
		mockService = mocks.DeviceService{}
		deviceHandler = handlers.NewDeviceHandlers(&mockService)
		response = httptest.NewRecorder()
		context, _ = gin.CreateTestContext(response)
	})

	var _ = Describe("GetAllDevices", func() {
		It("should return status code 200 when successful", func() {
			mockService.On("GetAllDevices").Return([]*models.Device{}, nil)
			deviceHandler.GetAllDevices(context)
			Expect(response.Code).To(Equal(200))
		})

		It("should return status code 500 when internal server error", func() {
			mockService.On("GetAllDevices").Return(nil, errors.New("Internal server errror"))
			deviceHandler.GetAllDevices(context)
			Expect(response.Code).To(Equal(500))
		})
	})

	var _ = Describe("GetByID", func() {
		It("should return status code 200 when successful", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			mockService.On("GetByID", testUUID).Return(&models.Device{}, nil)
			deviceHandler.GetByID(context)
			Expect(response.Code).To(Equal(200))
		})

		It("should return status code 400 when no uuid param passed", func() {
			mockService.On("GetByID", testUUID).Return(&models.Device{}, nil)
			deviceHandler.GetByID(context)
			Expect(response.Code).To(Equal(400))
		})

		It("should return status code 404 when device not found", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			mockService.On("GetByID", testUUID).Return(nil, errors.New("Not found"))
			deviceHandler.GetByID(context)
			Expect(response.Code).To(Equal(404))
		})
	})

	var _ = Describe("GetLatestByID", func() {
		It("should return status code 200 when successful", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			mockService.On("GetLatestByID", testUUID).Return(&models.LatestDevice{}, nil)
			deviceHandler.GetLatestByID(context)
			Expect(response.Code).To(Equal(200))
		})

		It("should return status code 400 when no uuid param passed", func() {
			mockService.On("GetLatestByID", testUUID).Return(&models.LatestDevice{}, nil)
			deviceHandler.GetLatestByID(context)
			Expect(response.Code).To(Equal(400))
		})

		It("should return status code 404 when device not found", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			mockService.On("GetLatestByID", testUUID).Return(nil, errors.New("Not found"))
			deviceHandler.GetLatestByID(context)
			Expect(response.Code).To(Equal(404))
		})
	})

	var _ = Describe("Update", func() {
		It("should return status code 200 when successful", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			context.Request, _ = http.NewRequest("PUT", "/", bytes.NewBufferString(`{"model_uuid": "00000000-0000-0000-0000-000000000000","device_name":"Test Device","metadata": {"foo":"bar"},"geoip": {"foo":"bar"}}`))
			context.Request.Header.Add("Content-Type", "application/json")
			mockService.On("GetByID", testUUID).Return(&models.Device{}, nil)
			mockService.On("Update", testUUID, &mocks.MockMetadataModel, &testDevice).Return(&models.Device{}, nil)
			deviceHandler.Update(context)
			Expect(response.Code).To(Equal(200))
		})

		It("should return status code 400 when uuid param is missing", func() {
			context.Params = []gin.Param{}
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			context.Request, _ = http.NewRequest("PUT", "/", bytes.NewBufferString(`{"model_uuid": "00000000-0000-0000-0000-000000000000","device_name":"Test Device","geoip": {"foo":"bar"}}`))
			context.Request.Header.Add("Content-Type", "application/json")
			mockService.On("GetByID", testUUID).Return(&models.Device{}, nil)
			mockService.On("Update", testUUID, &mocks.MockMetadataModel, &testDevice).Return(&models.Device{}, nil)
			deviceHandler.Update(context)
			Expect(response.Code).To(Equal(400))
		})

		It("should return status code 400 when device name missing from request", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			context.Request, _ = http.NewRequest("PUT", "/", bytes.NewBufferString(`{"model_uuid": "00000000-0000-0000-0000-000000000000","metadata": {"foo":"bar"},"geoip": {"foo":"bar"}}`))
			context.Request.Header.Add("Content-Type", "application/json")
			mockService.On("GetByID", testUUID).Return(&models.Device{}, nil)
			mockService.On("Update", testUUID, &mocks.MockMetadataModel, &testDevice).Return(&models.Device{}, nil)
			deviceHandler.Update(context)
			Expect(response.Code).To(Equal(400))
		})

		It("should return status code 400 when metadata missing from request", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			context.Request, _ = http.NewRequest("PUT", "/", bytes.NewBufferString(`{"model_uuid": "00000000-0000-0000-0000-000000000000","device_name":"Test Device","geoip": {"foo":"bar"}}`))
			context.Request.Header.Add("Content-Type", "application/json")
			mockService.On("GetByID", testUUID).Return(&models.Device{}, nil)
			mockService.On("Update", testUUID, &mocks.MockMetadataModel, &testDevice).Return(&models.Device{}, nil)
			deviceHandler.Update(context)
			Expect(response.Code).To(Equal(400))
		})
	})

	var _ = Describe("Delete", func() {
		It("should return status code 200 when successful", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			mockService.On("Delete", testUUID).Return(true, nil)
			deviceHandler.Delete(context)
			Expect(response.Code).To(Equal(200))
		})

		It("should return status code 400 when no uuid param passed", func() {
			mockService.On("Delete", uuid.Nil).Return(true, nil)
			deviceHandler.Delete(context)
			Expect(response.Code).To(Equal(400))
		})

		It("should return status code 404 when device not found", func() {
			context.Params = []gin.Param{
				gin.Param{
					Key:   "uuid",
					Value: testUUIDString,
				},
			}
			mockService.On("Delete", testUUID).Return(false, errors.New("Not found"))
			deviceHandler.Delete(context)
			Expect(response.Code).To(Equal(404))
		})
	})
})
