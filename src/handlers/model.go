package handlers

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/service"
	"bitbucket.innouk.music-group.com/cm/error-module-go/pkg/oops"
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
)

type ModelHandlers interface {
	GetAllModels(c *gin.Context)
	GetByID(c *gin.Context)
	Update(c *gin.Context)
	Delete(c *gin.Context)
	RegisterRoutes(r *gin.Engine, version int)
}

type modelHandlers struct {
	service services.ModelService
}

func NewModelHandlers(service services.ModelService) ModelHandlers {
	return &modelHandlers{
		service: service,
	}
}

func (h *modelHandlers) RegisterRoutes(r *gin.Engine, version int) {
	r.GET(fmt.Sprintf("/devices/v%d/models", version), h.GetAllModels)
	r.GET(fmt.Sprintf("/devices/v%d/models/:uuid", version), h.GetByID)
	r.PUT(fmt.Sprintf("/devices/v%d/models/:uuid", version), h.Update)
	r.DELETE(fmt.Sprintf("/devices/v%d/models/:uuid", version), h.Delete)
}

func (h *modelHandlers) GetAllModels(c *gin.Context) {
	models, err := h.service.GetAllModels()
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"models":  models,
	})
}

func (h *modelHandlers) GetByID(c *gin.Context) {
	modelUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Model UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	model, err := h.service.GetByID(modelUUID)
	if err != nil {
		oops.NewError(c.Writer, "Model not found", http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"model":   model,
	})
}

func (h *modelHandlers) Update(c *gin.Context) {
	modelUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Model UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	model := models.Model{}
	if err := c.ShouldBindJSON(&model); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
	}

	result, err := h.service.Update(modelUUID, &model)
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprint(err), http.StatusInternalServerError)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": true,
		"model":   result,
	})
}

func (h *modelHandlers) Delete(c *gin.Context) {
	modelUUID, err := uuid.Parse(c.Param("uuid"))
	if err != nil {
		oops.NewError(c.Writer, fmt.Sprintf("Model UUID is invalid: %s", err.Error()), http.StatusBadRequest)
		return
	}

	success, err := h.service.Delete(modelUUID)
	if err != nil {
		oops.NewError(c.Writer, "Model not found", http.StatusNotFound)
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"success": success,
	})
}
