package helpers

import (
	"crypto/tls"
	"net"
	"net/url"
	"time"

	"github.com/google/uuid"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
)

func ParseMongoURL(mongoURL string) (*mgo.DialInfo, error) {
	u, err := url.Parse(mongoURL)
	if err != nil {
		return nil, err
	}

	if sslValue, gotSSLValue := u.Query()["ssl"]; gotSSLValue && len(sslValue) == 1 && sslValue[0] == "true" {
		query := u.Query()
		query.Del("ssl")
		u.RawQuery = query.Encode()

		strippedUrl := u.String()

		dialInfo, err := mgo.ParseURL(strippedUrl)
		if err != nil {
			return nil, err
		}

		dialInfo.DialServer = func(addr *mgo.ServerAddr) (net.Conn, error) {
			return tls.Dial("tcp", addr.String(), &tls.Config{})
		}

		dialInfo.Timeout = time.Second * 5
		return dialInfo, nil
	} else {
		return mgo.ParseURL(mongoURL)
	}
}

func MongoUUID(id uuid.UUID) bson.Binary {
	bytes, err := id.MarshalBinary()
	if err != nil {
		panic(err)
	}

	// Kind 4 is "new UUID" format
	return bson.Binary{0, bytes}
}
