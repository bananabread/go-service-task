package models

import (
	"encoding/json"
	"github.com/google/uuid"
	"time"
)

type Device struct {
	UUID       uuid.UUID   `json:"uuid" bson:"_id,omitempty"`
	ModelUUID  uuid.UUID   `json:"model_uuid" bson:"model_uuid,omitempty"`
	DeviceName string      `json:"device_name" bson:"device_name" binding:"required"`
	Records    []*Metadata `json:"records" bson:"records,omitempty"`
	CreatedAt  time.Time   `json:"created_at" bson:"created_at"`
	UpdatedAt  time.Time   `json:"updated_at" bson:"updated_at"`
}

type LatestDevice struct {
	UUID       uuid.UUID `json:"uuid"`
	ModelUUID  uuid.UUID `json:"model_uuid"`
	DeviceName string    `json:"device_name"`
	Metadata   *Metadata `json:"metadata"`
	CreatedAt  time.Time `json:"created_at"`
	UpdatedAt  time.Time `json:"updated_at"`
}

type Metadata struct {
	Metadata json.RawMessage `json:"metadata" bson:"metadata" binding:"required"`
	Geoip    json.RawMessage `json:"geoip" bson:"geoip"`
}
