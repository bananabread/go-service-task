package models

import (
	"github.com/google/uuid"
	"time"
)

type Model struct {
	UUID      uuid.UUID `json:"uuid" bson:"_id,omitempty"`
	Type      string    `json:"type" bson:"type,omitempty" binding:"required"`
	Name      string    `json:"name" bson:"name" binding:"required"`
	CreatedAt time.Time `json:"created_at" bson:"created_at"`
	UpdatedAt time.Time `json:"updated_at" bson:"updated_at"`
}
