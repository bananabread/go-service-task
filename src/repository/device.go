package repositories

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/helpers"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"github.com/google/uuid"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

var deviceCollection = "devices"
var deviceDatabase = "devices"

type DeviceRepository interface {
	Fetch() ([]*models.Device, error)
	GetByID(uuid uuid.UUID) (*models.Device, error)
	Create(device *models.Device) (*models.Device, error)
	Update(uuid uuid.UUID, device *models.Device) (*models.Device, error)
	Delete(uuid uuid.UUID) (bool, error)
}

type deviceRepo struct {
	url     string
	session *mgo.Session
}

func NewDeviceMongoRepository(config *config.Config) DeviceRepository {
	dialInfo, err := helpers.ParseMongoURL(config.MongoURL)
	if err != nil {
		return nil
	}

	log.Printf("Opening Mongo connection at %v", config.MongoURL)
	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		return nil
	}

	return &deviceRepo{
		url:     config.MongoURL,
		session: session,
	}
}

func (r *deviceRepo) collection() *mgo.Collection {
	return r.session.DB(deviceDatabase).C(deviceCollection)
}

func (r *deviceRepo) Fetch() ([]*models.Device, error) {
	records := []*models.Device{}
	prototype := bson.M{}
	query := r.collection().Find(prototype)
	err := query.All(&records)
	if err != nil {
		return nil, err
	}
	return records, nil
}

func (r *deviceRepo) GetByID(uuid uuid.UUID) (*models.Device, error) {
	record := models.Device{}
	query := r.collection().FindId(helpers.MongoUUID(uuid))
	err := query.One(&record)
	if err != nil {
		return nil, err
	}
	return &record, nil
}

func (r *deviceRepo) Create(device *models.Device) (*models.Device, error) {
	err := r.collection().Insert(device)
	if err != nil {
		return nil, err
	}
	return device, nil
}

func (r *deviceRepo) Update(uuid uuid.UUID, device *models.Device) (*models.Device, error) {
	err := r.collection().UpdateId(helpers.MongoUUID(uuid), device)
	if err != nil {
		return nil, err
	}
	return device, nil
}

func (r *deviceRepo) Delete(uuid uuid.UUID) (bool, error) {
	err := r.collection().RemoveId(helpers.MongoUUID(uuid))
	if err != nil {
		return false, err
	}
	return true, nil
}
