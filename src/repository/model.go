package repositories

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/helpers"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"github.com/google/uuid"
	mgo "gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
)

var modelCollection = "models"
var modelDatabase = "devices"

type ModelRepository interface {
	Fetch() ([]*models.Model, error)
	GetByID(uuid uuid.UUID) (*models.Model, error)
	Create(model *models.Model) (*models.Model, error)
	Update(uuid uuid.UUID, model *models.Model) (*models.Model, error)
	Delete(uuid uuid.UUID) (bool, error)
}

type modelRepo struct {
	url     string
	session *mgo.Session
}

func NewModelMongoRepository(config *config.Config) ModelRepository {
	dialInfo, err := helpers.ParseMongoURL(config.MongoURL)
	if err != nil {
		return nil
	}

	log.Printf("Opening Mongo connection at %v", config.MongoURL)
	session, err := mgo.DialWithInfo(dialInfo)
	if err != nil {
		return nil
	}

	return &modelRepo{
		url:     config.MongoURL,
		session: session,
	}
}

func (r *modelRepo) collection() *mgo.Collection {
	return r.session.DB(modelDatabase).C(modelCollection)
}

func (r *modelRepo) Fetch() ([]*models.Model, error) {
	records := []*models.Model{}
	prototype := bson.M{}
	query := r.collection().Find(prototype)
	err := query.All(&records)
	if err != nil {
		return nil, err
	}
	return records, nil
}

func (r *modelRepo) GetByID(uuid uuid.UUID) (*models.Model, error) {
	record := models.Model{}
	query := r.collection().FindId(helpers.MongoUUID(uuid))
	err := query.One(&record)
	if err != nil {
		return nil, err
	}
	return &record, nil
}

func (r *modelRepo) Create(model *models.Model) (*models.Model, error) {
	err := r.collection().Insert(model)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (r *modelRepo) Update(uuid uuid.UUID, model *models.Model) (*models.Model, error) {
	err := r.collection().UpdateId(helpers.MongoUUID(uuid), model)
	if err != nil {
		return nil, err
	}
	return model, nil
}

func (r *modelRepo) Delete(uuid uuid.UUID) (bool, error) {
	err := r.collection().RemoveId(helpers.MongoUUID(uuid))
	if err != nil {
		return false, err
	}
	return true, nil
}
