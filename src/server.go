package devices

import (
	"fmt"

	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/handlers"
	"github.com/gin-gonic/gin"
	"gopkg.in/go-playground/validator.v9"
	"net/http"
)

type Service struct {
	router     *gin.Engine
	validate   *validator.Validate
	apiVersion int
	apiURL     string
	state      *bool
}

func New(dh handlers.DeviceHandlers, mh handlers.ModelHandlers) *Service {
	gin.SetMode(gin.ReleaseMode)

	state := true
	version := 1

	svc := Service{
		gin.Default(),
		validator.New(),
		version,
		"http://localhost:8000",
		&state,
	}

	dh.RegisterRoutes(&svc.router, version)
	mh.RegisterRoutes(&svc.router, version)

	return &svc
}

func (svc *Service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	fmt.Print(r.Method, " ", r.URL)
	svc.router.ServeHTTP(w, r)
}
