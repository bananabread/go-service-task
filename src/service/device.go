package services

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/repository"
	"github.com/google/uuid"
	"time"
)

type DeviceService interface {
	GetAllDevices() ([]*models.Device, error)
	GetByID(uuid uuid.UUID) (*models.Device, error)
	GetLatestByID(uuid uuid.UUID) (*models.LatestDevice, error)
	Update(uuid uuid.UUID, metadata *models.Metadata, device *models.Device) (*models.Device, error)
	Delete(uuid uuid.UUID) (bool, error)
}

type deviceService struct {
	repo repositories.DeviceRepository
}

func NewDeviceService(repo repositories.DeviceRepository) DeviceService {
	return &deviceService{
		repo: repo,
	}
}

func (s *deviceService) GetAllDevices() ([]*models.Device, error) {
	deviceList, err := s.repo.Fetch()
	if err != nil {
		return nil, err
	}
	return deviceList, nil
}

func (s *deviceService) GetByID(uuid uuid.UUID) (*models.Device, error) {
	response, err := s.repo.GetByID(uuid)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (s *deviceService) GetLatestByID(uuid uuid.UUID) (*models.LatestDevice, error) {
	response, err := s.repo.GetByID(uuid)
	if err != nil {
		return nil, err
	}

	latest := models.LatestDevice{
		UUID:       response.UUID,
		ModelUUID:  response.ModelUUID,
		DeviceName: response.DeviceName,
		Metadata:   response.Records[0],
		CreatedAt:  response.CreatedAt,
		UpdatedAt:  response.UpdatedAt,
	}

	return &latest, nil
}

func (s *deviceService) Update(uuid uuid.UUID, metadata *models.Metadata, device *models.Device) (*models.Device, error) {
	var response *models.Device

	exists, err := s.GetByID(uuid)
	if err != nil {
		// If the device does not yet exist
		device.UUID = uuid
		// Prepend the metadata to the start of the records array
		device.Records = append([]*models.Metadata{metadata}, device.Records...)
		device.CreatedAt = time.Now().UTC()
		device.UpdatedAt = time.Now().UTC()
		response, err = s.repo.Create(device)
		if err != nil {
			return nil, err
		}
	} else {
		// Otherwise update existing record
		exists.DeviceName = device.DeviceName
		exists.ModelUUID = device.ModelUUID
		// Prepend the metadata to the start of the records array
		exists.Records = append([]*models.Metadata{metadata}, exists.Records...)
		exists.UpdatedAt = time.Now().UTC()
		response, err = s.repo.Update(uuid, exists)
		if err != nil {
			return nil, err
		}
	}

	return response, nil
}

func (s *deviceService) Delete(uuid uuid.UUID) (bool, error) {
	success, err := s.repo.Delete(uuid)
	if err != nil {
		return false, err
	}
	return success, nil
}
