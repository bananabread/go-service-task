package services

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/repository"
	"github.com/google/uuid"
	"time"
)

type ModelService interface {
	GetAllModels() ([]*models.Model, error)
	GetByID(uuid uuid.UUID) (*models.Model, error)
	Update(uuid uuid.UUID, model *models.Model) (*models.Model, error)
	Delete(uuid uuid.UUID) (bool, error)
}

type modelService struct {
	repo repositories.ModelRepository
}

func NewModelService(repo repositories.ModelRepository) ModelService {
	return &modelService{
		repo: repo,
	}
}

func (s *modelService) GetAllModels() ([]*models.Model, error) {
	modelList, err := s.repo.Fetch()
	if err != nil {
		return nil, err
	}
	return modelList, nil
}

func (s *modelService) GetByID(uuid uuid.UUID) (*models.Model, error) {
	response, err := s.repo.GetByID(uuid)
	if err != nil {
		return nil, err
	}
	return response, nil
}

func (s *modelService) Update(uuid uuid.UUID, model *models.Model) (*models.Model, error) {
	var response *models.Model

	exists, err := s.GetByID(uuid)
	if err != nil {
		// If the model does not yet exist
		model.UUID = uuid
		model.CreatedAt = time.Now().UTC()
		model.UpdatedAt = time.Now().UTC()
		response, err = s.repo.Create(model)
		if err != nil {
			return nil, err
		}
	} else {
		// Otherwise update existing record
		exists.Name = model.Name
		exists.Type = model.Type
		exists.UpdatedAt = time.Now().UTC()
		response, err = s.repo.Update(uuid, exists)
		if err != nil {
			return nil, err
		}
	}

	return response, nil
}

func (s *modelService) Delete(uuid uuid.UUID) (bool, error) {
	success, err := s.repo.Delete(uuid)
	if err != nil {
		return false, err
	}
	return success, nil
}
