package services_test

import (
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/mocks"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/models"
	"bitbucket.innouk.music-group.com/cloud/mcloud-service-device-go/src/service"
	"errors"
	"github.com/google/uuid"
	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	"github.com/stretchr/testify/mock"

	"testing"
)

func TestService(t *testing.T) {
	RegisterFailHandler(Fail)
	RunSpecs(t, "Service Suite")
}

var _ = Describe("Device Service", func() {

	var (
		testUUID uuid.UUID
		// testUUIDString string
		mockRepository mocks.DeviceRepository
		deviceService  services.DeviceService
	)

	BeforeEach(func() {
		testUUID = uuid.New()
		// testUUIDString = testUUID.String()
		mockRepository = mocks.DeviceRepository{}
		deviceService = services.NewDeviceService(&mockRepository)
	})

	var _ = Describe("GetAllDevices", func() {
		It("should call repository Fetch and return slice of devices", func() {
			mockRepository.On("Fetch").Return([]*models.Device{}, nil)
			response, _ := deviceService.GetAllDevices()
			Expect(response).To(Equal([]*models.Device{}))
		})

		It("should return and error when repository Fetch fails", func() {
			mockRepository.On("Fetch").Return(nil, errors.New("Error"))
			_, err := deviceService.GetAllDevices()
			Expect(err).Should(HaveOccurred())
		})
	})

	var _ = Describe("GetByID", func() {
		It("should call repository GetByID and return a device", func() {
			mockRepository.On("GetByID", testUUID).Return(&models.Device{}, nil)
			response, _ := deviceService.GetByID(testUUID)
			Expect(response).To(Equal(&models.Device{}))
		})

		It("should return and error when repository GetByID fails", func() {
			mockRepository.On("GetByID", testUUID).Return(nil, errors.New("Error"))
			_, err := deviceService.GetByID(testUUID)
			Expect(err).Should(HaveOccurred())
		})
	})

	var _ = Describe("GetLatestByID", func() {
		It("should call repository GetByID and return a latest device", func() {
			mockRepository.On("GetByID", testUUID).Return(&models.Device{
				Records: []*models.Metadata{
					&mocks.MockMetadataModel,
				},
			}, nil)
			response, _ := deviceService.GetLatestByID(testUUID)
			Expect(response).To(Equal(&models.LatestDevice{
				Metadata: &mocks.MockMetadataModel,
			}))
		})

		It("should return error when repository GetByID fails", func() {
			mockRepository.On("GetByID", testUUID).Return(nil, errors.New("Not Found"))
			_, err := deviceService.GetLatestByID(testUUID)
			Expect(err).Should(HaveOccurred())
		})
	})

	var _ = Describe("Update", func() {
		It("should call repository create when device does not exist and return device", func() {
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			mockRepository.On("GetByID", testUUID).Return(nil, errors.New("Not Found"))
			mockRepository.On("Create", mock.Anything).Return(&models.Device{}, nil)
			response, _ := deviceService.Update(testUUID, &models.Metadata{}, &models.Device{})
			Expect(response).Should(Equal(&models.Device{}))
		})

		It("should call repository update when device already exists and return device", func() {
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			mockRepository.On("GetByID", testUUID).Return(&models.Device{}, nil)
			mockRepository.On("Update", testUUID, mock.Anything).Return(&models.Device{}, nil)
			response, _ := deviceService.Update(testUUID, &models.Metadata{}, &models.Device{})
			Expect(response).Should(Equal(&models.Device{}))
		})

		It("should return error when repository create fails", func() {
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			mockRepository.On("GetByID", testUUID).Return(nil, errors.New("Not Found"))
			mockRepository.On("Create", mock.Anything).Return(nil, errors.New("Error"))
			_, err := deviceService.Update(testUUID, &models.Metadata{}, &models.Device{})
			Expect(err).Should(HaveOccurred())
		})

		It("should return error when repository update fails", func() {
			testDevice := mocks.MockDeviceModel
			testDevice.ModelUUID = uuid.Nil
			testDevice.UUID = uuid.Nil
			mockRepository.On("GetByID", testUUID).Return(&models.Device{}, nil)
			mockRepository.On("Update", testUUID, mock.Anything).Return(nil, errors.New("Error"))
			_, err := deviceService.Update(testUUID, &models.Metadata{}, &models.Device{})
			Expect(err).Should(HaveOccurred())
		})
	})

	var _ = Describe("Delete", func() {
		It("should call repository Delete and return true", func() {
			mockRepository.On("Delete", testUUID).Return(true, nil)
			response, _ := deviceService.Delete(testUUID)
			Expect(response).To(Equal(true))
		})

		It("should return and error when repository Delete fails", func() {
			mockRepository.On("Delete", testUUID).Return(false, errors.New("Error"))
			_, err := deviceService.Delete(testUUID)
			Expect(err).Should(HaveOccurred())
		})
	})

})
